import subprocess
from datetime import datetime

DateFormat = '%B'

with open('psykopaattikoodi.tex','r') as f:
    data = f.read()

CommitId = subprocess.check_output(['git','rev-parse','HEAD']).decode('utf-8')[0:-1]

Date = subprocess.check_output(['git','show','-s','--format=%ci','HEAD']).decode('utf-8')[0:-1]
#Date = datetime.strptime(Date[0:10],'%Y-%m-%d').strftime(DateFormat)

VersionNumber = subprocess.check_output(['git','rev-list','--count','HEAD']).decode('utf-8')[0:-1]

data = data.replace('___CommitId___',CommitId)
data = data.replace('___Date___',Date)
data = data.replace('___VersionNumber___',VersionNumber)

print(data)
