# Psykopaattikoodi

Tämä on kirjan _The Psychopath Code_ (Pieter Hintjens 2015) suomennoksen (Konsta Kurki) lähdekoodi. Suomennos käännetään pdf-tiedostoksi ajamalla skripti `build.sh`. Uusi commit tarkoittaa uutta versiota suomennoksesta; commit josta suomennos on buildattu on näkyvillä pdf-tiedoston alussa. Versionumero määräytyy siitä, monesko commit on kyseessä.

* [Uusin versio](http://konstakurki.org/psykopaattikoodi.pdf)

Suomennoksesta on mahdollista tilata paperikopio. Tilausohjeet ja yhteystiedot löytyvät suomennoksen alusta.

---

Copyright &copy; (2014-2019) Pieter Hintjens, Konsta Kurki

Lisenssi: [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
